﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Correio
{
    /* Programa que calcula a melhor rota entre duas cidades, calculando todas as possíveis e armazenando somente a melhor rota */
    /* Autor: Merielen dos Santos Guidolini */
    /* Data: 12/08/2017*/
    public class Program
    {
        // Definição das estruturas de dados e variáveis globais
        public struct Route
        {
            public string startCity { get; set; }
            public string endCity { get; set; }
            public int days { get; set; }
        }

        public struct BestRoute
        {
            public string startCity { get; set; }
            public string endCity { get; set; }
            public List<Route> routes { get; set; }
            public int daysTotal { get; set; }
        }

        static public Dictionary<int, string> cities = new Dictionary<int, string>();
        static public int[,] Paths;
        static public BestRoute bestRoute;
        static List<BestRoute> bestRoutes;

        static void Main(string[] args)
        {
            FillPaths();
            Paths = new int[cities.Count, cities.Count];

            ReadPaths();
            string[] orders = ReadOrders();

            bestRoutes = new List<BestRoute>();

            foreach (string order in orders)
            {
                string startCity = order.Substring(0, 2);
                string endCity = order.Substring(3, 2);
                bestRoute = new BestRoute();

                CalculateBestRoute(startCity, endCity, new List<Route>());
                bestRoutes.Add(bestRoute);

                int daysTotal = bestRoute.daysTotal;
            }

            WriteBestRoutes();

            Console.Write("Rotas calculadas com sucesso!");
            Console.ReadLine();
        }

        // Lê arquivo de entrada contendo as rotas possíveis assim como o número de dias gastos
        static public void ReadPaths()
        {
            try
            {
                string[] lines = System.IO.File.ReadAllLines("../../input/trechos.txt");
                {
                    foreach (string line in lines)
                    {
                        string startCity = line.Substring(0, 2);
                        string endCity = line.Substring(3, 2);
                        int days = Convert.ToInt16(line.Substring(6, 1));

                        Paths[cities.FirstOrDefault(c => c.Value == startCity).Key, cities.FirstOrDefault(c => c.Value == endCity).Key] = days;
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write("Erro ao ler o arquivo trechos.txt. \n" + e.Message);
                Console.ReadLine();
            }
        }

        // Lê arquivo de entrada contendo as encomendas
        static public string[] ReadOrders()
        {
            try
            {
                return System.IO.File.ReadAllLines("../../input/encomendas.txt");
            }
            catch (Exception e)
            {
                Console.Write("Erro ao ler o arquivo encomendas.txt. \n" + e.Message);
                Console.ReadLine();
                return null;
            }
        }

        // Preenche as cidades e condados
        static public void FillPaths()
        {
            cities.Add(0, "LS");
            cities.Add(1, "SF");
            cities.Add(2, "LV");
            cities.Add(3, "RC");
            cities.Add(4, "WS");
            cities.Add(5, "BC");
        }

        // Calcula a melhor rota entre duas cidades e salva na variável global "bestRoute" quando encontra
        static public void CalculateBestRoute(string startCity, string endCity, List<Route> routes)
        {
            try
            {
                for (int i = 0; i < cities.Count; i++)
                {
                    int days = Paths[cities.FirstOrDefault(c => c.Value == startCity).Key, i];

                    if (days > 0 && routes.Where(r => r.endCity == cities[i] || r.startCity == cities[i]).Count() == 0)
                    {
                        Route route = new Route();
                        route.endCity = cities[i];
                        route.startCity = startCity;
                        route.days = days;
                        routes.Add(route);

                        if (cities[i] == endCity)
                        {
                            if (bestRoute.daysTotal == 0 || bestRoute.daysTotal > routes.Sum(r => r.days))
                            {
                                bestRoute.routes = new List<Route>(routes);
                                bestRoute.daysTotal = routes.Sum(r => r.days);
                                bestRoute.startCity = routes.Select(r => r.startCity).FirstOrDefault();
                                bestRoute.endCity = routes.Select(r => r.endCity).LastOrDefault();

                                routes.Remove(routes.Last());
                            }
                        }
                        else
                        {
                            CalculateBestRoute(cities[i], endCity, routes);
                        }
                    }
                }

                if (routes.Count() > 0)
                    routes.Remove(routes.Last());
            }
            catch (Exception e)
            {
                Console.Write("Erro ao calcular a melhor rota entre duas cidades. \n" + e.Message);
                Console.ReadLine();
            }
        }

        // Escreve no arquivo de saída as melhores rotas encontradas para cada encomenda
        static public void WriteBestRoutes()
        {
            try
            {
                // Arquivo de saída definido na pasta output/rotas.txt
                System.IO.StreamWriter file = new System.IO.StreamWriter("../../output/rotas.txt");

                foreach (BestRoute br in bestRoutes)
                {
                    foreach (Route r in br.routes)
                    {
                        if (br.routes.First().Equals(r))
                            file.Write(r.startCity + " " + r.endCity + " ");
                        else
                            file.Write(r.endCity + " ");
                        if (br.routes.Last().Equals(r))
                            file.Write(br.daysTotal + "\r\n");
                    }
                }

                file.Close();
            }
            catch(Exception e)
            {
                Console.Write("Erro ao gravar as melhores rotas no arquivo rotas.txt. \n" + e.Message);
                Console.ReadLine();
            }
        }

    }
}
