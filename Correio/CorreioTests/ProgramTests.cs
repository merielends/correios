﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Correio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correio.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod()]
        public void CalculateBestRouteTest()
        {
            Dictionary<int, string> cities = new Dictionary<int, string>();
            int[,] Paths;

            string startCity = "WS";
            string endCity = "BC";
            int daysExpected = 5;

            Correio.Program.cities = cities;
            Correio.Program.FillPaths();
            Paths = new int[cities.Count, cities.Count];
            Correio.Program.Paths = Paths;
            Correio.Program.ReadPaths();

            Correio.Program.CalculateBestRoute(startCity, endCity, new List<Program.Route>());

            Assert.AreEqual(Correio.Program.bestRoute.daysTotal, daysExpected);

        }
    }
}